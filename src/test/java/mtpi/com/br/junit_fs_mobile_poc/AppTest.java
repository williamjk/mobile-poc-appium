package mtpi.com.br.junit_fs_mobile_poc;

import java.io.File;
import java.io.IOException;
import java.net.URL;

import org.apache.commons.exec.ExecuteException;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.remote.DesiredCapabilities;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class AppTest {

	static String deviceName = "Google Nexus 4 - 5.1.0 -API 22-768x1280";
	AppiumDriver<WebElement> driver;
	DesiredCapabilities capabilities;

	@Before
	public void setUp() throws InterruptedException, ExecuteException, IOException {

		capabilities = new DesiredCapabilities(); 

		File dir = new File("C:/Users/guilherme.cutovoi/Documents/Cucumber-gui/apps/android");
		File app = new File(dir, "vivo-sync-1-4-4.apk");

		capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
		capabilities.setCapability(MobileCapabilityType.PLATFORM_NAME, "Android");
		capabilities.setCapability(MobileCapabilityType.DEVICE_NAME, "AlemasGui"); // "Google
																					// Nexus
																					// 4
																					// -
																					// 5.1.0
																					// -
																					// API
																					// 22
																					// -
																					// 768x1280"
		capabilities.setCapability("platformVersion", "6.0 Marshmallow (API Level 23)");
		capabilities.setCapability(MobileCapabilityType.PLATFORM, MobilePlatform.ANDROID);

		driver = new AppiumDriver<WebElement>(new URL("http://127.0.0.1:4723/wd/hub"), capabilities) {

			public WebElement scrollTo(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

			public WebElement scrollToExact(String arg0) {
				// TODO Auto-generated method stub
				return null;
			}

		};

	}

	@Test
	public void mobileApptest() throws InterruptedException {

		Thread.sleep(10000);
		// Given I'm in app
		Assert.assertTrue(driver.findElement(By.xpath("//android.view.View[@content-desc='Vivo Sync']")).isDisplayed());
		// And click the 'Continue' button
		Thread.sleep(3000);
		driver.findElement(By.xpath("//android.view.View[@content-desc='Continuar']")).click();
		// Then the button 'Vivo Movel' appears
		Thread.sleep(20000);
		Assert.assertTrue(
				driver.findElement(By.xpath("//android.widget.Button[@content-desc='Vivo Móvel']")).isDisplayed());
		Thread.sleep(2000);
		// And click the 'Vivo Movel' button
		driver.findElement(By.xpath("//android.widget.Button[@content-desc='Vivo Móvel']")).click();
		// Then the field 'Insira seu numero Vivo' is showed
		Assert.assertTrue(driver.findElement(By.xpath("//android.widget.EditText[@resource-id='phone'")).isDisplayed());
		// And insert "myNumber" on the field 'Insira seu numero Vivo'
		driver.findElement(By.xpath("//android.widget.EditText[@resource-id='phone'")).sendKeys("31995613013");
		// And click the 'Entrar' button
		driver.findElement(By.xpath("//android.view.View[@content-desc='Entrar']")).click();
		// Then the confirm message apears
		Assert.assertTrue(driver
				.findElement(
						By.xpath("//android.view.View[@content-desc='Foi este o número que você digitou? Confirme!']"))
				.isDisplayed());
		// And check "myNumber" on the screen
		Assert.assertTrue(driver.findElement(By.xpath("//android.widget.EditText[@resource-id='numero_confirmacao']"))
				.getText().equalsIgnoreCase("31995613013"));
		// And click the 'Este e seu celular' button
		driver.findElement(By.xpath("//android.view.View[@content-desc='Este é seu celular? Close']")).click();
		// Then the field 'Digite sua senha' appears
		Assert.assertTrue(
				driver.findElement(By.xpath("//android.widget.EditText[@resource-id='password']")).isDisplayed());

	}

	@After
	public void tearDown() throws Exception {
		driver.quit();

	}

}
